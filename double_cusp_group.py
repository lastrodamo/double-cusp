#----------------- DOUBLE CUSP GROUP - PYTHON ------------------#
#------------------------ version 0.0.5 ------------------------#
#																#
#--------------- Based on Double Cusp Group --------------------# 
#--------- by Paul Nylander, bugman123.com, 6/29/06 ------------#
#																#
#------------- DoubleCusp.pov of bugman123.com -----------------#
#---------- Converted to python by Damien Monteillard ----------#
#																#
#                      Damien Monteillard                       #
#                             (2018)                            #
#																#
#------------ GNU General Public License v3.0 only -------------#
#------------------------- GPL-3.0 -----------------------------#
#																#


bl_info = {
    "name": "Double Cusp Group",
    "author": "Damien Monteillard (lastrodamo)",
    "version": (0, 0, 5),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh",
    "description": "Double Cusp Group",
    "warning": "",
    "wiki_url": "https://gitlab.com/lastrodamo/double-cusp",
    "tracker_url": "",
    "category": "Mesh"}

import math
import cmath
import random
import numpy as np
from math import *
from cmath import *
import bpy
from bpy.props import (
	BoolProperty,
	EnumProperty,
	FloatProperty,
	FloatVectorProperty,
	IntProperty
	)

def double_cusp_group(self, context):
	scene=bpy.context.scene
	ri=float(self.ric) # Transformation ta
	reel=float(self.reel_of_tb) # Rotation reel_of_tb
	rc=float(self.ryc) # Rayon
	n=int(self.number_of_iterations) # Number of iterations

	# Basic founctions
	def Square(X): 
		return X*X

	# Complex Algebra
	def Complex(X,Y):
		return complex(X,Y)

	I=complex(0,1) # > 1j
	print("Valeur de I ", I)

	def Re(Z):
		return Z.real
	#print("Reel of Z = ", Re(Z))

	def Im(Z):
		return Z.imag
	#print("Imaginary of Z = ", Im(Z))

	def Conjugate(Z):
		return Complex(Re(Z),-Im(Z)) #Z.conjugate()
	#print("Conjugate of Z = ", Conjugate(Z))

	def Abs(Z):
		return abs(Z) #cmath.sqrt(Square(Re(Z))+Square(Im(Z)))
	#print("Absolute value of Z = ", abs(Z))

	def Arg(Z):
		return atan2(Im(Z),Re(Z))
	#print("Argument of Z = ", Arg(Z))

	def Sqr(Z):
		return Complex(cmath.sqrt(Re(Z))-cmath.sqrt(Im(Z)),2*Re(Z)*Im(Z))
	#print("Square of Z = ", Sqr(Z))

	def Pow(Z,n):
		r=Abs(Z)
		if(r==0):
			z2=Complex(0,0)
		else:
			theta=n*Arg(Z)
			z2=pow(r,n)*Complex(cos(theta),sin(theta))
		return z2
		
	#print("Power of Z,n = ", Pow(Z,n))

	def Exp(Z):
		return exp(Re(Z))*Complex(cos(Im(Z)),sin(Im(Z)))
	#print("Exp of Z = ", Exp(Z))

	def Sqrt(Z):
		return Pow(Z,1/2)
	#print("Sqrt of Z = ", Sqrt(Z))

	def Mult(z1,z2):
		return Complex(Re(z1)*Re(z2)-Im(z1)*Im(z2),Im(z1)*Re(z2)+Re(z1)*Im(z2))
	#print("Sqrt of z1, z2 = ", Mult(z1,z2))

	def Div(z1,z2):
		return Mult(z1,Pow(z2,-1))
	#print("Division of z1, z2 = ", Div(z1,z2))



	#2×2 Matrix Algebra
	def Matrix(a,b,c,d):
		return np.array([[a, b], [c, d]])
		#return a.transpose()

	def Conjugate2(A):
		return Matrix(Conjugate(A[0][0]),Conjugate(A[0][1]),Conjugate(A[1][0]),Conjugate(A[1][1]))
	#print("Conjugate 2", Conjugate2(A))

	def Det(A):
		return Mult(A[0][0],A[1][1])-Mult(A[0][1],A[1][0])

	def Mult2(Z,A):
		return Matrix(Mult(A[0][0],Z),Mult(A[0][1],Z),Mult(A[1][0],Z),Mult(A[1][1],Z))

	def Div2(A,Z):
		return Matrix(Div(A[0][0],Z),Div(A[0][1],Z),Div(A[1][0],Z),Div(A[1][1],Z))

	def Inverse(A):
		return Div2(Matrix(A[1][1],-A[0][1],-A[1][0],A[0][0]),Det(A))

	def Dot(A1,A2):
		return Matrix(Mult(A1[0][0],A2[0][0])+Mult(A1[0][1],A2[1][0]),Mult(A1[0][0],A2[0][1])+Mult(A1[0][1],A2[1][1]),Mult(A2[0][0],A1[1][0])+Mult(A2[1][0],A1[1][1]),Mult(A2[0][1],A1[1][0])+Mult(A1[1][1],A2[1][1]))

	def Pow2(A1,n):
		A2=A1
		i=1
		while(i<n):
			A2=Dot(A2,A1)
			i=i+1
		return A2

	#Calculations

	#declarations
	ta=Complex(1.958591030-ri,-0.011278560-ri)
	tb=Complex(reel,0) # 2-(ri*10)
	tab=(Mult(ta,tb)+Sqrt(Mult(Sqr(ta),Sqr(tb))-4*(Sqr(ta)+Sqr(tb))))/2
	z0=Div(Mult(tab-Complex(2,0),tb),Mult(tb,tab)-2*ta+2*Mult(I,tab))
	b=Matrix((tb-2*I)/2,tb/2,tb/2,(tb+2*I)/2);
	B=Inverse(b);
	a=Dot(Matrix(tab,Div(tab-Complex(2,0),z0),Mult(tab+Complex(2,0),z0),tab),B)
	A=Inverse(a)

	#definition
	def Fix(A):
		return Div(A[0][0]-A[1][1]-Sqrt(4*Mult(A[0][1],A[1][0])+Sqr(A[0][0]-A[1][1])),2*A[1][0])

	def ToMatrix(Z,r):
		return Mult2(I/r,Matrix(Z,Complex(Square(r),0)-Mult(Z,Conjugate(Z)),Complex(1,0),-Conjugate(Z)))

	def MotherCircle(M1,M2,M3):
		z1=Fix(M1)
		x1=Re(z1)
		y1=Im(z1)
		z2=Fix(M2)
		x2=Re(z2)
		y2=Im(z2)
		z3=Fix(M3)
		x3=Re(z3)
		y3=Im(z3)
		z0=Complex(Square(x3)*(y1-y2)+(Square(x1)+(y1-y2)*(y1-y3))*(y2-y3)+Square(x2)*(y3-y1),-Square(x2)*x3+Square(x1)*(x3-x2)+x3*(y1-y2)*(y1+y2)+x1*(Square(x2)-Square(x3)+Square(y2)-Square(y3))+x2*(Square(x3)-Square(y1)+Square(y3)))/(2*(x3*(y1-y2)+x1*(y2-y3)+x2*(y3-y1)))
		return ToMatrix(z0,Abs(z1-z0))

	C1=MotherCircle(b,Dot(a,Dot(b,A)),Dot(a,Dot(b,Dot(A,B))))
	C2=MotherCircle(Dot(b,Pow2(a,15)),Dot(a,Dot(b,Pow2(a,14))),Dot(a,Dot(b,Dot(A,B))))

	def Reflect(C,M):
		return Dot(M,Dot(C,Inverse(Conjugate2(M))))

	def zcen(A):
		return Div(A[0][0],A[1][0])

	def rad(A):
		return Re(Div(I,A[1][0]))



	#Sphere Spirals
	def spiral(C0,M,n,colorize,reverse):
		C=C0
		i=0
		while(i<n-1):
			Z=zcen(C)
			r=rad(C)
			def polar(Z):
				return cmath.polar(Z)
	#		print("Polar Coordinates: ", cmath.polar(Z))
			rho = abs(Z)
			phi = phase(Z)
	#		print("Polar radius :", rho)
	#		print("Polar angle :", phi)

			cx = rho * np.cos(phi)
			cy = rho * np.sin(phi)
	#		print("Sur x :", cx)
	#		print("Sur y :", cy)

			def rounded():
				bpy.ops.mesh.primitive_ico_sphere_add(radius=r*rc*0.5, align='WORLD', enter_editmode=False, location=(cx*0.5, cy*0.5, 0))
				#bpy.ops.object.subdivision_set(level=0)
				#bpy.context.object.modifiers["Subsurf"].render_levels = 1
				#bpy.ops.object.shade_smooth()
				#bpy.ops.object.lamp_add(type='POINT', radius=r*rc*0.5, view_align=False, location=(cx*0.5, cy*0.5, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

			rounded()

			#print("Valeurs du rayon r : ", r)
			#print("Valeurs de Z : ", Z)
			C=Reflect(C,M)
			i=i+1

	spiral(C1,a,n,1,0)
	spiral(C1,A,n,1,1)
	#spiral(C2,a,91,0,0)
	#spiral(C2,A,76,0,1)


class Double_cusp_group_button(bpy.types.Panel):
	bl_idname = "Double_cusp_group_button"
	bl_label = "Double cusp group"
	bl_category = "Double cusp"
	bl_space_type = "VIEW_3D"
	bl_region_type = 'UI'

	def draw(self, context):
		layout = self.layout
		layout.operator("object.double_cusp_group")

#    def execute(self, context):
#        pass # More code here
#        return {'FINISHED'}


class Double_cusp_group(bpy.types.Operator):
	bl_idname = "object.double_cusp_group"
	bl_label = "Double cusp group"
	bl_description = "Create an Double cusp group"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	reel_of_tb=FloatProperty(
		name="Transform b",
		description="Rotation - reel of tb",
		default=2.00, min=0.10, max=10.00, soft_min=0.01, step=5
		)
	ric=FloatProperty(
		name="Transform a",
		description="ta",
		default=0.01, min=0.01, max=1.00, soft_min=0.01, step=0.1
		)
	ryc=FloatProperty(
		name="Radius",
		description="Radius of the spheres",
		default=0.707, min=0.01, max=4.00, soft_min=0.01, step=1
		)
	number_of_iterations=IntProperty(
		name="Iterations",
		description="Number of iterations",
		default=20, min=15, max=91, step=10
		)

	def draw(self, context):
		layout = self.layout
		col = layout.column()
		col.label(text='Transform b')
		col.prop(self, 'reel_of_tb')
		col.label(text='Transform a')
		col.prop(self, 'ric')
		col.label(text='Iterations')
		col.prop(self, 'number_of_iterations')
		col.label(text='Radius')
		col.prop(self, 'ryc')

	def execute(self, context):
		double_cusp_group(self, context)
		return {'FINISHED'}

classes = (
    Double_cusp_group_button,
    Double_cusp_group,
)

register, unregister = bpy.utils.register_classes_factory(classes)
#register, unregister = bpy.utils.register_classes_factory(Double_cusp_group)


#def unregister():
#	bpy.utils.unregister_class(Double_cusp_group_button)
#	bpy.utils.unregister_class(Double_cusp_group)

# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
	register()
