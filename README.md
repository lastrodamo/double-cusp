# Double cusp v0.0.5 - blender addon

## Synopsis

Double cusp is an blender python adaptation of Paul Nylander, [bugman123.com](http://bugman123.com) Double cusp.

This addon generate a mesh with icospheres on a double spiral.

## Installation

1. Open blender 2.80 RC 2
2. File User > Preferences > Addon
3. Install from files search double_cusp.py
4. Search double and check the button of Mesh: Double Cusp Group and Save user settings

## Experiments examples - demo files

![alt text](http://www.3dminfographie.com/images/scripts/double-cusp/double_cusp_dc00-00.png "Doublecusp DC00")
![alt text](http://www.3dminfographie.com/images/scripts/double-cusp/double_cusp_dc01-00.png "Doublecusp DC01")
![alt text](http://www.3dminfographie.com/images/scripts/double-cusp/double_cusp_dc02-00.png "Doublecusp DC02")

## Tests

On 3D viewport, N panel search Double Cusp panel and execute your first Double cusp group

Experiments with Transform b and a, iterations and radius

demo and how to on this link

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/r9gAAraXENQ/0.jpg)](https://www.youtube.com/watch?v=r9gAAraXENQ)

## License

GNU General Public License v3.0 only - GPL-3.0 - Damien Monteillard - 2018
